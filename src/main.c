//Gabriella Kupsho and Emily Anderson
/******************************************************************************************************************************/
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>
#include <zephyr/drivers/adc.h> 
#include <zephyr/drivers/pwm.h>
#include "pressure.h"
#include <nrfx_power.h> // for VBUS - NOTE: This is not a Zephyr library!!  It is a Nordic NRFX library.
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>

LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);
/******************************************************************************************************************************/
// Our Macros <3
#define HEARTBEAT_INTERVAL_MS 1000 // define heartbeat interval (used to start heartbeat timer)
#define ERROR_INTERVAL_MS 1000 // define error interval (used to start error timer in error state)
#define LEDS_INTERVAL_MS 1000 // define leds interval (used to start leds 1 and 2)
#define MEASUREMENT_DELAY_MS 1000  // delay between measurements
#define OVERSAMPLE 10  // number of samples to average together
#define IM_AN_IDIOT 100 // array length for 100 Hz signal
#define IM_AN_IDIOT_TOO 500 // array length for 500 Hz signal
#define PERIOD_ARRAY 10 // run through arrays this many times

int interval_size = 100;

#define ADC_DT_SPEC_GET_BY_ALIAS(vadc0)                    \
{                                                            \
    .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(vadc0))),      \
    .channel_id = DT_REG_ADDR(DT_ALIAS(vadc0)),            \
    ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(vadc0))        \
}                                                            \

#define BT_UUID_REMOTE_SERV_VAL \
        BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_SERVICE 			BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

/* UUID of the Data Characteristic */
#define BT_UUID_REMOTE_DATA_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_DATA_CHRC 		BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

/* UUID of the Message Characteristic */
#define BT_UUID_REMOTE_MESSAGE_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0002, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_MESSAGE_CHRC 	BT_UUID_DECLARE_128(BT_UUID_REMOTE_MESSAGE_CHRC_VAL) 

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
#define BLE_DATA_POINTS 600 // limited by MTU
/*****************************************************************************************************************************/
//define variables as integers
int32_t val_mv0; //channel 0 adc value (mv)
int32_t val_mv1; //channnel 1 adc value (mv)
int32_t val_mv2; //channel 2 adc value (mv)

int64_t hb_wq = 0; // used in the heartbeat timer work handler
int64_t hb_exec_time = 0; // used in the heartbeat timer handler

int64_t error_wq = 0; // used in the error timer work handler
int64_t error_exec_time = 0; // used in the error timer handler

static int32_t atm_pressure_kPa; 

int i; // for led1
int j; // for led2

int e; // for led2
int g; // for led2

int period; // for led1
int period2; // for led2

float array_data_led1[IM_AN_IDIOT] = {};
float max_data_array1[PERIOD_ARRAY] = {};
float min_data_array1[PERIOD_ARRAY] = {};

float array_data_led2[IM_AN_IDIOT_TOO] = {};
float max_data_array2[PERIOD_ARRAY] = {};
float min_data_array2[PERIOD_ARRAY] = {};

 // vbus
int64_t usbregstatus;

//battery
int battery_level; 
int normalized_battery_level;

//bluetooth
enum bluetooth_outputs {led1_brightness, led2_brightness, pressure_reading};
int16_t bluetooth_output[3];
/**********************************************************************************************************************************/
//BLUETOOTH 

// enumeration to keep track of the state of the notifications
enum bt_data_notifications_enabled {
    BT_DATA_NOTIFICATIONS_ENABLED,
    BT_DATA_NOTIFICATIONS_DISABLED,
};
enum bt_data_notifications_enabled notifications_enabled;

// declare struct of the BLE remove service callbacks
struct bt_remote_srv_cb {
    void (*notif_changed)(enum bt_data_notifications_enabled status);
    void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
}; 

struct bt_remote_srv_cb remote_service_callbacks;

// blocking thread semaphore to wait for BLE to initialize
static K_SEM_DEFINE(bt_init_ok, 1, 1);

/* Advertising data */
static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
};

/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
};

/**********************************************************************************************************************************/
// giving the leds firmware variable names, calling their aliases in the devicetree overlay, and storing that info in a structure
static const struct gpio_dt_spec low_range_led = GPIO_DT_SPEC_GET(DT_ALIAS(led1), gpios);
static const struct gpio_dt_spec high_range_led = GPIO_DT_SPEC_GET(DT_ALIAS(led2), gpios);
static const struct gpio_dt_spec error_led = GPIO_DT_SPEC_GET(DT_ALIAS(error), gpios);
static const struct gpio_dt_spec heartbeat_led = GPIO_DT_SPEC_GET(DT_ALIAS(heartbeat), gpios);

// giving the buttons firmware variable names, calling their aliases in the devicetree overlay, and storing that info in a structure
static const struct gpio_dt_spec save_button = GPIO_DT_SPEC_GET(DT_ALIAS(button1), gpios);
static const struct gpio_dt_spec bluetooth_button = GPIO_DT_SPEC_GET(DT_ALIAS(button2), gpios);

// ADC stuff
static const struct adc_dt_spec adc_vadc0_low_range = ADC_DT_SPEC_GET_BY_ALIAS(vadc0); // calling channel 0 alias from devicetree
static const struct adc_dt_spec adc_vadc1_high_range = ADC_DT_SPEC_GET_BY_ALIAS(vadc1); // calling channel 1 alias from devicetree
static const struct adc_dt_spec adc_vadc2_battery = ADC_DT_SPEC_GET_BY_ALIAS(vadc2); // calling channel 2 alias from devicetree

// PWM stuff
static const struct pwm_dt_spec led1_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm1));
static const struct pwm_dt_spec led2_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm2));

//initialize states structure 
static const struct smf_state lab_states[];

//initialize states
// INIT STATE
static void init_entry(void *o);
static void init_run(void *o);
static void init_exit(void *o);

// IDLE STATE
static void idle_entry(void *o);
static void idle_run(void *o);

// ACTION STATE
static void action_entry(void *o);
static void action_run(void *o);
static void action_exit(void *o);

// ERROR STATE #1 (PRESSURE)
static void pressure_error_entry(void *o);
static void pressure_error_run(void *o);
static void pressure_error_exit(void *o);

// ERROR STATE #2 (VBUS)
static void vbus_error_entry(void *o);
static void vbus_error_run(void *o);
static void vbus_error_exit(void *o);

// honeywell_mpr is a default device in the Zephyr ecosystem
const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr); 
/****************************************************************************************************************************/
// Define structs to hold callback information 
static struct gpio_callback save_data_cb; // button 1 press callback
static struct gpio_callback bluetooth_cb; // button 2 press callback
/*******************************************************************************************************************************/
// Callback Function Declarations (just saying "hey this is what you should expect to see when I call this callback")

// putting the raw to mv conversion into a function
void measure1(void); 
void measure2(void);
void measure3(void);

// heartbeat timer handler and work handler
void heartbeat_timer_handler(struct k_timer *heartbeat_timer);
void heartbeat_timer_work_handler(struct k_work *heartbeat_timer_work);

// leds 1 timer handler and work handler
void leds_timer_on_handler (struct k_timer *leds_timer);
void leds_timer_on_work_handler (struct k_work *leds_timer_toggled_on); 
void leds_timer_off_handler (struct k_timer *leds_timer);
void leds_timer_off_work_handler (struct k_work *leds_timer_toggled_off); 

// leds 2 timer handler and work handler
void leds2_timer_on_handler (struct k_timer *leds2_timer);
void leds2_timer_on_work_handler (struct k_work *leds2_timer_toggled_on); 
void leds2_timer_off_handler (struct k_timer *leds2_timer);
void leds2_timer_off_work_handler (struct k_work *leds2_timer_toggled_off);

// error led timer handler and work handler
void error_timer_handler(struct k_timer *error_timer);
void error_timer_work_handler(struct k_work *error_timer_work);

// define save button
void save_button_callback(const struct device *dev, struct gpio_callback*cb, uint32_t pins);
void save_button_callback_work_handler(struct k_work *save_button_callback_work);

// define bluetooth button
void bluetooth_button_callback(const struct device *dev, struct gpio_callback*cb, uint32_t pins);
void bluetooth_button_callback_work_handler(struct k_work *bluetooth_button_callback_work);

//MORE BLUETOOTH
/* Function Declarations Bluetooth*/
static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
void bluetooth_set_battery_level(int level, int nominal_batt_mv);
uint8_t bluetooth_get_battery_level(void);
int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length);
/* Function Declarations */
static struct bt_conn *current_conn;
void on_connected(struct bt_conn *conn, uint8_t ret);
void on_disconnected(struct bt_conn *conn, uint8_t reason);
void on_notif_changed(enum bt_data_notifications_enabled status);
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);

/* BLE Callback Functions */
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

    notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

    if (remote_service_callbacks.notif_changed) {
        remote_service_callbacks.notif_changed(notifications_enabled);
    }
}

static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &bluetooth_output, sizeof(bluetooth_output));
}

static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
    LOG_INF("Received data, handle %d, conn %p", attr->handle, (void *)conn);
    
    if (remote_service_callbacks.data_rx) {
        remote_service_callbacks.data_rx(conn, buf, len);
    }
    return len;
}

void on_sent(struct bt_conn *conn, void *user_data) {
    ARG_UNUSED(user_data);
    LOG_INF("Notification sent on connection %p", (void *)conn);
}

void bt_ready(int ret) {
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
    }

    // release the thread once initialized
    k_sem_give(&bt_init_ok);
}

/* Setup BLE Services */
BT_GATT_SERVICE_DEFINE(remote_srv,
BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC,
                BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                BT_GATT_PERM_READ,
                read_data_cb, NULL, NULL),
BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_MESSAGE_CHRC,
                BT_GATT_CHRC_WRITE_WITHOUT_RESP,
                BT_GATT_PERM_WRITE,
                NULL, on_write, NULL),
);

int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length) {
    int ret = 0;

    struct bt_gatt_notify_params params = {0};
    const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

    params.attr = attr;
    params.data = &value;
    params.len = length;
    params.func = on_sent;

    ret = bt_gatt_notify_cb(conn, &params);

    return ret;
}

/* Initialize the BLE Connection */
int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
    LOG_INF("Initializing Bluetooth");

    if ((bt_cb == NULL) | (remote_cb == NULL)) {
        return -NRFX_ERROR_NULL;
    }
    bt_conn_cb_register(bt_cb);
    remote_service_callbacks.notif_changed = remote_cb->notif_changed;
    remote_service_callbacks.data_rx = remote_cb->data_rx;

    int ret = bt_enable(bt_ready);
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
        return ret;
    }

 // hold the thread until Bluetooth is initialized
	k_sem_take(&bt_init_ok, K_FOREVER);

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		settings_load();
	}

	ret = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	if (ret) {
		LOG_ERR("Could not start advertising (ret = %d)", ret);
		return ret;
	}

	return ret;
}
/* Setup Callbacks */
struct bt_conn_cb bluetooth_callbacks = {
    .connected = on_connected,
    .disconnected = on_disconnected,
};

struct bt_remote_srv_cb remote_service_callbacks = {
    .notif_changed = on_notif_changed,
    .data_rx = on_data_rx,
};


void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
    uint8_t temp_str[len+1];
    memcpy(temp_str, data, len);
    temp_str[len] = 0x00; // manually append NULL character at the end

    LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
    LOG_INF("Data: %s", temp_str);
}

void on_connected(struct bt_conn *conn, uint8_t ret) {
    if (ret) { LOG_ERR("Connection error: %d", ret); }
    LOG_INF("BT connected");
    current_conn = bt_conn_ref(conn);
}

void on_disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_INF("BT disconnected (reason: %d)", reason);
    if (current_conn) {
        bt_conn_unref(current_conn);
        current_conn = NULL;
    }
}

void on_notif_changed(enum bt_data_notifications_enabled status) {
    if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
        LOG_INF("BT notifications enabled");
    }
    else {
        LOG_INF("BT notifications disabled");
    }
}
 
 void measure1(void)
{    // preparing channel 0
    int16_t buf0;   

    struct adc_sequence sequence0 = 
    {
    .buffer = &buf0,
    .buffer_size = sizeof(buf0), // bytes
    };
    LOG_INF("Measuring %s (channel %d)... ", adc_vadc0_low_range.dev->name, adc_vadc0_low_range.channel_id);

    (void)adc_sequence_init_dt(&adc_vadc0_low_range, &sequence0);

    int ret0;
    ret0 = adc_read(adc_vadc0_low_range.dev, &sequence0);
    if 
    (ret0 < 0) 
    {
    LOG_ERR("Could not read (%d)", ret0);
    } 
    else {LOG_DBG("Raw ADC Buffer: %d", buf0);}

    val_mv0 = buf0;
    ret0 = adc_raw_to_millivolts_dt(&adc_vadc0_low_range, &val_mv0); // remember that the vadc struct containts all the DT parameters
    if 
    (ret0 < 0) 
    {
    LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } 
    else {LOG_INF("ADC Value (mV): %d", val_mv0);}
}

void measure2(void)
{ 
// preparing channel 1
    int16_t buf1;   

    struct adc_sequence sequence1 = 
    {
    .buffer = &buf1,
    .buffer_size = sizeof(buf1), // bytes
    };
    LOG_INF("Measuring %s (channel %d)... ", adc_vadc1_high_range.dev->name, adc_vadc1_high_range.channel_id);

    (void)adc_sequence_init_dt(&adc_vadc1_high_range, &sequence1);

    int ret1;
    ret1 = adc_read(adc_vadc1_high_range.dev, &sequence1);
    if 
    (ret1 < 0) 
    {
    LOG_ERR("Could not read (%d)", ret1);
    } 
    else {LOG_DBG("Raw ADC Buffer: %d", buf1);}

    val_mv1 = buf1;
    ret1 = adc_raw_to_millivolts_dt(&adc_vadc1_high_range, &val_mv1); // remember that the vadc struct containts all the DT parameters
    if 
    (ret1 < 0) 
    {
    LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } 
    else {LOG_INF("ADC Value (mV): %d", val_mv1);}

}

void measure3(void){
    // preparing channel 2
    int16_t buf2;

    struct adc_sequence sequence2 = 
    {
    .buffer = &buf2,
    .buffer_size = sizeof(buf2), // bytes
    };
    LOG_INF("Measuring %s (channel %d)... ", adc_vadc2_battery.dev->name, adc_vadc2_battery.channel_id);

    (void)adc_sequence_init_dt(&adc_vadc2_battery, &sequence2);

    int ret2;
    ret2 = adc_read(adc_vadc2_battery.dev, &sequence2);
    if 
    (ret2 < 0) 
    {
    LOG_ERR("Could not read (%d)", ret2);
    } 
    else {LOG_DBG("Raw ADC Buffer: %d", buf2);}

    val_mv2 = buf2;
    ret2 = adc_raw_to_millivolts_dt(&adc_vadc2_battery, &val_mv2); // remember that the vadc struct containts all the DT parameters
    if 
    (ret2 < 0) 
    {
    LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } 
    else {LOG_INF("ADC Value (mV): %d", val_mv2);}
}

/*****************************************************************************************************************************/
K_TIMER_DEFINE (heartbeat_timer, heartbeat_timer_handler, NULL); // macro that sets up the heartbeat timer for me (name, what to do when toggled on, what to do when toggled off)
K_TIMER_DEFINE (leds_timer, leds_timer_on_handler, leds_timer_off_handler); // macro that sets up the action led timer (name, what to do when toggled on, what to do when toggled off)
K_TIMER_DEFINE (leds2_timer, leds2_timer_on_handler, leds2_timer_off_handler); 
K_TIMER_DEFINE (error_timer, error_timer_handler, NULL);

K_WORK_DEFINE(heartbeat_timer_work, heartbeat_timer_work_handler);
K_WORK_DEFINE(leds_timer_toggled_on, leds_timer_on_work_handler);
K_WORK_DEFINE(leds_timer_toggled_off, leds_timer_off_work_handler);
K_WORK_DEFINE(error_timer_work, error_timer_work_handler);

K_WORK_DEFINE(leds2_timer_toggled_on, leds2_timer_on_work_handler);
K_WORK_DEFINE(leds2_timer_toggled_off, leds2_timer_off_work_handler);

K_WORK_DEFINE(save_button_callback_work, save_button_callback_work_handler); // setting up save button

K_WORK_DEFINE(bluetooth_button_callback_work, bluetooth_button_callback_work_handler); // setting up bluetooth button
/*****************************************************************************************************************************/
//Declare State Table ENUM

enum lab_state {init, idle, action, pressure_error, vbus_error};
struct s_object 
    {
        struct smf_ctx ctx;
	} s_obj;

static const struct smf_state lab_states[]=
{
[init] = SMF_CREATE_STATE(init_entry, init_run, init_exit),
[idle] = SMF_CREATE_STATE(idle_entry, idle_run, NULL),
[action] = SMF_CREATE_STATE(action_entry, action_run, action_exit ),
[pressure_error] = SMF_CREATE_STATE(pressure_error_entry, pressure_error_run, pressure_error_exit ),
[vbus_error] = SMF_CREATE_STATE(vbus_error_entry, vbus_error_run, vbus_error_exit),
};
/*****************************************************************************************************************************/
//INIT STATE - does all the stuff that you do only once in the beginning

static void init_entry(void *o)
{
    int ret;
// check the 4 leds
	if (!device_is_ready(heartbeat_led.port)) 
        {
			return ret; // check heartbeat led is ready
		}
    if (!device_is_ready(low_range_led.port)) 
        {
			return ret; // check low range led is ready
		}
    if (!device_is_ready(high_range_led.port)) 
        {
			return ret; // check high range led is ready
		}
    if (!device_is_ready(error_led.port)) 
        {
			return ret; // error led is ready
		}
// check the 3 channels
    if (!device_is_ready(adc_vadc0_low_range.dev)) 
        {
		LOG_ERR("ADC controller device(s) not ready");
		return -1; // check channel 0 is ready
	    }
	if (!device_is_ready(adc_vadc1_high_range.dev)) 
        {
		LOG_ERR("ADC controller device(s) not ready");
		return -1; // check channel 1 is ready
	    }
	if (!device_is_ready(adc_vadc2_battery.dev)) 
        {
		LOG_ERR("ADC controller device(s) not ready");
		return -1; // check channel 1 is ready
	    }
    if (!device_is_ready(led1_pwm.dev))  
        {
        LOG_ERR("PWM device %s is not ready.", led1_pwm.dev->name);
        return -1;
	    }
    if (!device_is_ready(led2_pwm.dev))  
        {
        LOG_ERR("PWM device %s is not ready.", led2_pwm.dev->name);
        return -1;
	    }
    if (!device_is_ready(pressure_in)) {
		LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
        LOG_INF("press in %lld", pressure_in);
		return;
    }
    else {
        LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
    }
// set up the 3 channels
	int err;
	err = adc_channel_setup_dt(&adc_vadc0_low_range);
	if (err < 0) 
        {
		LOG_ERR("Could not setup ADC channel (%d)", err);
		return err;
	    }
	err = adc_channel_setup_dt(&adc_vadc1_high_range);
	if (err < 0) 
        {
		LOG_ERR("Could not setup ADC channel (%d)", err);
		return err;
	    }
	err = adc_channel_setup_dt(&adc_vadc2_battery);
	if (err < 0) 
        {
		LOG_ERR("Could not setup ADC channel (%d)", err);
		return err;
	    }
    err = pwm_set_pulse_dt(&led1_pwm, led1_pwm.period); // 100% duty cycle
    if (err) 
        {
        LOG_ERR("Could not set pwm led1 (PWM1)");
        }
    err = pwm_set_pulse_dt(&led2_pwm, led2_pwm.period); // 100% duty cycle
    if (err) 
        {
        LOG_ERR("Could not set pwm led2 (PWM2)");
        }
// configure leds as output pins
    ret = gpio_pin_configure_dt(&low_range_led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) 
        {
		return ret;
	    }
    ret = gpio_pin_configure_dt(&high_range_led, GPIO_OUTPUT_ACTIVE); 
	if (ret < 0) 
        {
		return ret;
	    }
    ret = gpio_pin_configure_dt(&error_led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) 
        {
		return ret;
	    }
	ret = gpio_pin_configure_dt(&heartbeat_led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) 
        {
		return ret;
	    }
    // configure buttons as user input pins
    ret = gpio_pin_configure_dt(&save_button, GPIO_INPUT); 
	if (ret < 0) 
        {
	     return ret;
	    }
    ret = gpio_pin_configure_dt(&bluetooth_button, GPIO_INPUT); 
	if (ret < 0) 
        {
	     return ret;
        }  
    LOG_INF("Init entry");
    
    /* Initialize Bluetooth */
    int bluetooth_err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
    if (bluetooth_err) {
        LOG_ERR("BT init failed (err = %d)", bluetooth_err);
    }
}
static void init_run(void *o)
{
    smf_set_state(SMF_CTX(&s_obj), &lab_states[idle]); //go to idle state
    LOG_INF("Init run");
}

static void init_exit(void *o)
{
    // start heartbeat timer (LED 4)
    k_timer_start(&heartbeat_timer, K_MSEC(HEARTBEAT_INTERVAL_MS), K_MSEC(HEARTBEAT_INTERVAL_MS)); 
    LOG_INF("Heartbeat pumpin - init exit");
}
/*****************************************************************************************************************************/
// IDLE STATE - just waits for button press 

static void idle_entry(void *o)
{
  // initialize callback functions: buttons 1 and 2 (measure pressure, save all data, send bluetooth notification)
    int ret_cb; 

    //button 1 - save
    ret_cb = gpio_pin_interrupt_configure_dt(&save_button, GPIO_INT_EDGE_TO_ACTIVE);
    gpio_init_callback(&save_data_cb, save_button_callback, BIT(save_button.pin));
    gpio_add_callback(save_button.port, &save_data_cb);

    //button 2 - bluetooth
    ret_cb = gpio_pin_interrupt_configure_dt(&bluetooth_button, GPIO_INT_EDGE_TO_ACTIVE);
    gpio_init_callback(&bluetooth_cb, bluetooth_button_callback, BIT(bluetooth_button.pin));
    gpio_add_callback(bluetooth_button.port, &bluetooth_cb);
    LOG_INF("Idle entry");

    gpio_pin_set_dt(&error_led, 0);
    gpio_pin_set_dt(&low_range_led, 0);
    gpio_pin_set_dt(&high_range_led, 0);
}

static void idle_run(void *o)
{
    usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
       if ((usbregstatus) == 1)
        {
          smf_set_state(SMF_CTX(&s_obj), &lab_states[vbus_error]);
        } 

     float atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
                k_msleep(MEASUREMENT_DELAY_MS);
                if (atm_pressure_kPa <= 0)
                {
                    smf_set_state(SMF_CTX(&s_obj), &lab_states[pressure_error]);
                        bluetooth_output[pressure_reading] = atm_pressure_kPa;
                } 
                LOG_INF("Pressure value: %f", atm_pressure_kPa);   

}
/*****************************************************************************************************************************/
// ACTION STATE

static void action_entry(void *o)
{

   gpio_pin_interrupt_configure_dt(&save_button, GPIO_INT_DISABLE); // disable save button while current data is being saved
   LOG_INF("Action entry");
}
static void action_run(void *o)
{
  LOG_INF("action run");
}
static void action_exit(void *o)
    {
        LOG_INF("Action exit");
        k_timer_stop(&leds_timer);
        k_timer_stop(&leds2_timer);
        smf_set_state(SMF_CTX(&s_obj), &lab_states[idle]);
    }

/*****************************************************************************************************************************/
// PRESSURE ERROR STATE

static void pressure_error_entry(void *o)
{
    k_timer_stop(&leds_timer);
    k_timer_stop(&leds2_timer);
    gpio_pin_set_dt(&error_led, 1);
    LOG_INF("Error LED on");

    gpio_pin_interrupt_configure_dt(&save_button, GPIO_INT_DISABLE); // disable save button when callback ISR occurs and leaving this state
    gpio_pin_interrupt_configure_dt(&bluetooth_button, GPIO_INT_DISABLE); // disable bluetooth button when callback ISR occurs and leaving this state
    LOG_INF("pressure error entry");
}

static void pressure_error_run(void *o)
{
       float atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0); 
       if (atm_pressure_kPa > 0)
        {
                smf_set_state(SMF_CTX(&s_obj), &lab_states[idle]);
        } 
}

static void pressure_error_exit(void *o)
{
    gpio_pin_set_dt(&error_led, 0);

    // restart leds timer (LEDs 1 and 2)
    k_timer_start(&leds_timer, K_MSEC(LEDS_INTERVAL_MS),K_MSEC(LEDS_INTERVAL_MS));
    k_timer_start(&leds2_timer, K_MSEC(LEDS_INTERVAL_MS),K_MSEC(LEDS_INTERVAL_MS));
    LOG_INF("Pressure Error Off, Ready for Input Voltages /n");
}
/*****************************************************************************************************************************/
//VBUS ERROR STATE

static void vbus_error_entry(void *o)
{
    k_timer_stop(&leds_timer);
    k_timer_stop(&leds2_timer);

    // start error timer (LED 3)
    k_timer_start(&error_timer, K_MSEC(ERROR_INTERVAL_MS), K_MSEC(ERROR_INTERVAL_MS)); 
    LOG_INF("Error LED blinking");

    gpio_pin_interrupt_configure_dt(&save_button, GPIO_INT_DISABLE); // disable save button when callback ISR occurs and leaving this state
    gpio_pin_interrupt_configure_dt(&bluetooth_button, GPIO_INT_DISABLE); // disable bluetooth button when callback ISR occurs and leaving this state
    LOG_INF("vbus error entry");
}
static void vbus_error_run(void *o)
{
   usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
       if ((usbregstatus) == 0)
        {
          smf_set_state(SMF_CTX(&s_obj), &lab_states[idle]);
        } 
}

static void vbus_error_exit(void *o)
{
    k_timer_stop(&error_timer);
    LOG_INF("vbus error exit");
}

/*****************************************************************************************************************************/
// Callback Function Definitions (actually telling the code what to do when each callback is initialized)

//HEARTBEAT TIMER
void heartbeat_timer_handler(struct k_timer *heartbeat_timer)
{
k_work_submit(&heartbeat_timer_work);
hb_exec_time = k_uptime_get() - hb_wq;
}

void heartbeat_timer_work_handler (struct k_work *heartbeat_timer_work)
{
gpio_pin_toggle_dt(&heartbeat_led);
hb_wq = k_uptime_get();
}

// LED 1 TIMER

void leds_timer_on_handler (struct k_timer *leds_timer)
{
k_work_submit(&leds_timer_toggled_on);
}

void leds_timer_on_work_handler (struct k_work *leds_timer_toggled_on)
{
    //create array, for led 1
   if (i < (IM_AN_IDIOT))
   {
    measure1 ();
    array_data_led1[i++] = val_mv0;
   }
    else if(i = (IM_AN_IDIOT))
    {
        LOG_INF("Array is full for led1, timer stops");
        LOG_INF("i %ld", i);
        k_timer_stop(&leds_timer);
        LOG_INF("Contents of array_data_led1:");

        for (int i = 0; i < (IM_AN_IDIOT); i++) 
        {
        LOG_INF("array_data_led1[%d] = %f", i, array_data_led1[i]);
        }
    }
}

void leds_timer_off_handler (struct k_timer *leds_timer)
{
k_work_submit(&leds_timer_toggled_off);
}

void leds_timer_off_work_handler (struct k_work *leds_timer_toggled_off) 
{ 
    float min_value_led1;
    float max_value_led1;
//LED 1: get min and max after 1 sec (array is full now)
    if (period < (PERIOD_ARRAY))
    {
        period++;
        min_value_led1 = array_data_led1[i]; //setting first number of array as the min
        max_value_led1 = array_data_led1[i]; //setting first number of array as the max

        for (int j = 0; j < (IM_AN_IDIOT); j++) 
            {
                if (array_data_led1[j] < min_value_led1) 
                { /// iterate through array to find min
                min_value_led1 = array_data_led1[j];
                }
                if (array_data_led1[j] > max_value_led1) 
                { //iterate through array to find max
                max_value_led1 = array_data_led1[j];
                }
            }
    
    LOG_INF("LED 1 Max Value: %f", max_value_led1);
	LOG_INF("LED 1 Min Value: %f", min_value_led1);
    LOG_INF("period: %d", period);

    max_data_array1[j] = max_value_led1;
    min_data_array1[j] = min_value_led1;
    j++;

    i=0;
    k_timer_start(&leds_timer, K_MSEC(IM_AN_IDIOT), K_MSEC(IM_AN_IDIOT));
    }
	else if (period = (PERIOD_ARRAY))
    {
        LOG_INF("Calculate averages of min and max");
        for (int i = 0; i < (IM_AN_IDIOT); i++)
        {
            LOG_INF("max data led1 [%d] = %f", i, max_data_array1[i]);
            LOG_INF("min data led1 [%d] = %f", i, min_data_array1[i]);
        }
        float sum_max = 0;
        float sum_min = 0;

        for (int i = 0; i < IM_AN_IDIOT; i++)
        {
            sum_max += max_data_array1[i];
            LOG_INF("sum max 1 %f", sum_max);
        }
        for (int i = 0; i < IM_AN_IDIOT; i++)
        {    
            sum_min += min_data_array1[i];
            LOG_INF("sum min 1%f", sum_min);
        }

        float average_max;
        average_max = sum_max / (IM_AN_IDIOT);
        LOG_INF("max average %f", average_max);

        float average_min;
        average_min = sum_min / (IM_AN_IDIOT);
        LOG_INF("min average %f", average_min);

        float led1_vpp = average_max - average_min;
        LOG_INF("LED 1 VPP %f", led1_vpp);
        
        float led1_brightness = ((1000000*(led1_vpp+5)))/(50+5);
        float led1_brightness_percentage = led1_brightness/10000;
        LOG_INF("Led 1 Brightness: %f", led1_brightness);
        LOG_INF("Brightness Percentage: %f", led1_brightness_percentage);
        pwm_set_pulse_dt(&led1_pwm, (int)led1_brightness);
        smf_set_state(SMF_CTX(&s_obj), &lab_states[idle]);
    }
} 


// LED 2 TIMER
void leds2_timer_on_handler (struct k_timer *leds2_timer)
{
k_work_submit(&leds2_timer_toggled_on);
}

void leds2_timer_on_work_handler (struct k_work *leds2_timer_toggled_on)
{
    //create array, for led 2
   if (e < (IM_AN_IDIOT_TOO))
   {
    measure2 ();
    array_data_led2[e++] = val_mv1;
   }
    else if(e = (IM_AN_IDIOT_TOO))
    {
        LOG_INF("Array is full for led2, timer stops");
        LOG_INF("e %ld", e);
        k_timer_stop(&leds2_timer);
        LOG_INF("Contents of array_data_led2:");

        for (int e = 0; e < (IM_AN_IDIOT); e++) 
        {
        LOG_INF("array_data_led2[%d] = %f", e, array_data_led2[e]);
        }
    } 
}

void leds2_timer_off_handler (struct k_timer *leds2_timer)
{
k_work_submit(&leds2_timer_toggled_off);

}

void leds2_timer_off_work_handler (struct k_work *leds2_timer_toggled_off) 
{ 
    float min_value_led2;
    float max_value_led2;
//LED 2: get min and max after 1 sec (array is full now)
    if (period2 < (PERIOD_ARRAY))
    {
        period2++;
        min_value_led2 = array_data_led2[e]; //setting first number of array as the min
        max_value_led2 = array_data_led2[e]; //setting first number of array as the max

        for (int g = 0; g < (IM_AN_IDIOT_TOO); g++) 
            {
                if (array_data_led2[g] < min_value_led2) 
                { /// iterate through array to find min
                min_value_led2 = array_data_led2[g];
                }
                if (array_data_led2[g] > max_value_led2) 
                { //iterate through array to find max
                max_value_led2 = array_data_led2[g];
                }
            }
    
    LOG_INF("LED 2 Max Value: %f", max_value_led2);
	LOG_INF("LED 2 Min Value: %f", min_value_led2);
    LOG_INF("period2: %d", period2);

    max_data_array2[g] = max_value_led2;
    min_data_array2[g] = min_value_led2;
    g++;

    e=0;
    k_timer_start(&leds2_timer, K_MSEC(IM_AN_IDIOT), K_MSEC(IM_AN_IDIOT));
    }
	else if (period2 = (PERIOD_ARRAY))
    {
        LOG_INF("Calculate averages of led 2 min and max");
        for (int e = 0; e < (IM_AN_IDIOT_TOO); e++)
        {
            LOG_INF("max data led2 [%d] = %f", e, max_data_array2[e]);
            LOG_INF("min data led2 [%d] = %f", e, min_data_array2[e]);
        }
        float sum_max2 = 0;
        float sum_min2 = 0;

        for (int e = 0; e < IM_AN_IDIOT_TOO; e++)
        {
            sum_max2 += max_data_array2[e];
            LOG_INF("sum max2 %f", sum_max2);
        }
        for (int e = 0; e < IM_AN_IDIOT_TOO; e++)
        {    
            sum_min2 += min_data_array2[e];
            LOG_INF("sum min2 %f", sum_min2);
        }

        float average_max2;
        average_max2 = sum_max2 / (IM_AN_IDIOT_TOO);
        LOG_INF("max average 2 %f", average_max2);

        float average_min2;
        average_min2 = sum_min2 / (IM_AN_IDIOT_TOO);
        LOG_INF("min average 2 %f", average_min2);

        float led2_vpp = average_max2 - average_min2;
        LOG_INF("LED 2 VPP %f", led2_vpp);
        
        float led2_brightness = ((1000000*(led2_vpp+10)))/(150+10);
        float led2_brightness_percentage = led2_brightness/10000;
        LOG_INF("Led 2 Brightness: %f", led2_brightness);
        LOG_INF("Brightness Percentage LED 2: %f", led2_brightness_percentage);
        pwm_set_pulse_dt(&led2_pwm, (int)led2_brightness);
        smf_set_state(SMF_CTX(&s_obj), &lab_states[idle]);
    }
} 

// error led timer
void error_timer_handler(struct k_timer *error_timer)
{
k_work_submit(&error_timer_work);
error_exec_time = k_uptime_get() - error_wq;
}

void error_timer_work_handler (struct k_work *error_timer_work)
{
gpio_pin_toggle_dt(&error_led);
error_wq = k_uptime_get();
}

//SAVE DATA CALLBACK
void save_button_callback(const struct device *dev, struct gpio_callback*cb, uint32_t pins)
    {
        LOG_INF("Save data button has been pressed! Reading data.... \n");
        k_work_submit(&save_button_callback_work);
    }
void save_button_callback_work_handler(struct k_work*save_button_callback_work)
    {
        //reset data array
        bluetooth_output[led1_brightness] = 0;
        bluetooth_output[led2_brightness] = 0;
        bluetooth_output[pressure_reading] = 0;
        
        k_timer_start(&leds_timer, K_MSEC(IM_AN_IDIOT),K_MSEC(IM_AN_IDIOT));
        k_timer_start(&leds2_timer, K_MSEC(IM_AN_IDIOT),K_MSEC(IM_AN_IDIOT));

        
        period = 0;
        smf_set_state(SMF_CTX(&s_obj), &lab_states[action]); //go to action state 
    }

//BLUETOOTH CALLBACK
void bluetooth_button_callback(const struct device *dev, struct gpio_callback*cb, uint32_t pins)
{
        LOG_INF("Bluetooth button has been pressed! \n");
        k_work_submit(&bluetooth_button_callback_work);
}
void bluetooth_button_callback_work_handler(struct k_work*bluetooth_button_callback_work)
{

     /* Example of how to use the Battery Service
       `normalized_level` comes from an ADC reading of the battery voltage
       this function populates the BAS GATT with information
    */
    measure3();
    float battery_percent = ((100*val_mv2)/3600);
    LOG_INF("Battery_percent: %f",  battery_percent);
    
    //normalized level will be from adc - in action entry need some sort of equation
    int battery_err = bt_bas_set_battery_level((int)battery_level);
    if (battery_err) {
        LOG_ERR("BAS set error (err = %d)", battery_err);
    }

    battery_level =  bt_bas_get_battery_level();

// used to test bluetooth
    bluetooth_output[led1_brightness] = 31;
    bluetooth_output[led2_brightness] = 69;
    bluetooth_output[pressure_reading] = 102;

    LOG_INF("first array: (%d)", bluetooth_output[led1_brightness]);
    LOG_INF("second array: (%d)", bluetooth_output[led2_brightness]);
    LOG_INF("third array: (%d)",  bluetooth_output[pressure_reading]);
    
    int batt_err = send_data_notification(current_conn, bluetooth_output, 3);
    if (batt_err = 0) {
        LOG_ERR("Could not send BT notification (err: %d)", batt_err);
    }
    else {
        LOG_INF("BT data transmitted.");
    }
}

//******************************************************************************************************************************/
// all main loop does is tell you which state to run
void main(void) 
{
int64_t ret;
	smf_set_initial(SMF_CTX(&s_obj), &lab_states[init]); // set initial state
// Run the state machine
        while(1) 
        {
                // State machine terminates if a non-zero value is returned 
                ret = smf_run_state(SMF_CTX(&s_obj));
                if (ret) {
                        // handle return code and terminate state machine 
                        break;
                         }
        }
}
